package exercice.command;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * Objet qui rassemble toutes les informations sur une commande et permet de
 * générer la syntaxe d'utilisation. Peut contenir des sous-commandes, et par
 * conséquent des commandes parents.
 * 
 * @author Quentin
 *
 */
public class Command {

	private Command parent;
	private final String commandDesc;

	private final Map<String, Command> subCommands;
	private final Set<String> args;
	private final Set<String> optionalArgs;

	private final int minArgs;
	private final int maxArgs;

	private final CommandExecutor executor;

	/**
	 * Constructeur
	 * 
	 * @param commandDesc  La description de la commande.
	 * @param args         Les arguments que la commande doit demander.
	 * @param optionalArgs Les arguments optionels que la commande peut comprendre.
	 * @param executor     L'Interface Fonctionnelle permettant de contenir
	 *                     l'action.
	 */
	public Command(String commandDesc, Set<String> args, Set<String> optionalArgs, CommandExecutor executor) {
		this.commandDesc = commandDesc;
		this.subCommands = new HashMap<>();
		this.args = args;
		this.optionalArgs = optionalArgs;
		this.minArgs = args == null ? 0 : args.size();
		this.maxArgs = minArgs + (optionalArgs == null ? 0 : optionalArgs.size());
		this.executor = executor;
	}

	/**
	 * Une méthode pour générer automatiquement la syntaxe en fonction des
	 * paramètres donnés dans le constructeur
	 */
	public String generateSyntax() {
		if (minArgs == 0) {
			return new String();
		}
		StringBuilder syntax = new StringBuilder();
		for (String arg : args) {
			syntax.append("<");
			syntax.append(arg);
			syntax.append("> ");
		}

		if (!optionalArgs.isEmpty()) {
			syntax.append("[");
			for (String arg : optionalArgs) {

				syntax.append(arg);
				syntax.append("|");
			}
			syntax.delete(syntax.length() - 1, syntax.length());
			syntax.append("] ");
		}

		return syntax.toString();
	}

	/**
	 * Ajouter une sous-commande.
	 * 
	 * @param name    Le nom de la commande.
	 * @param command L'objet représentant la commande.
	 */
	public void addSubCommand(String name, Command command) {
		subCommands.put(name, command);
		command.setParent(this);
	}

	/**
	 * Définir un parent à la commande.
	 * 
	 * @param command L'objet représentant le nouveau parent.
	 */
	private void setParent(Command command) {
		this.parent = command;
	}

	/**
	 * Un objet Command pour récupérer la commande parent, cet objet peut être null.
	 * 
	 * @return L'objet Command représentant le parent, peut être {@code}null.
	 * 
	 */
	public Command getParent() {
		return parent;
	}

	/**
	 * La description de la commande, avec une valeur par défaut.
	 * 
	 * @return La description de la commande.
	 */
	public String getCommandDesc() {
		return commandDesc;
	}

	/**
	 * Une liste des sous-commandes (avec l’objet Command)
	 * 
	 * @return La liste des sous-commandes.
	 */
	public Map<String, Command> getSubCommands() {
		return subCommands;
	}

	/**
	 * Une liste de string pour les arguments obligatoires.
	 * 
	 * @return La liste des arguments demandés pour le fonctionnement de la
	 *         commande.
	 */
	public Set<String> getArgs() {
		return args;
	}

	/**
	 * Une liste de string pour les arguments optionnels.
	 * 
	 * @return La liste des arguments optionels pouvant être indiqués à la commande.
	 */
	public Set<String> getOptionalArgs() {
		return optionalArgs;
	}

	/**
	 * Le nombre minimal d’arguments (dois être généré automatiquement en fonction
	 * du nombre d’arguments obligatoires et optionnels)
	 * 
	 * @return Le nombre minimum d'arguments nécéssaires pour utiliser la commande.
	 */
	public int getMinArgs() {
		return minArgs;
	}

	/**
	 * Le nombre maximum d’arguments (doit être généré automatiquement en fonction
	 * du nombre d’arguments obligatoires et optionnels)
	 * 
	 * @return Le nombre maximum d'arguments que la commande peut lire.
	 */
	public int getMaxArgs() {
		return maxArgs;
	}

	/**
	 * Récupérer l'action contenue à executer via l'Interface Fonctionnelle.
	 * 
	 * @return L'instance de l'executor.
	 */
	public CommandExecutor getExecutor() {
		return executor;
	}
}
