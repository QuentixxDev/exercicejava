package exercice.command;

/**
 * Interface Fonctionnelle utilisée pour contenir les actions d'une commande qui
 * seront executées lorsque la commande sera appelée.
 * 
 * @author Quentin
 *
 */
@FunctionalInterface
public interface CommandExecutor {

	public void execute(String... args);
}
