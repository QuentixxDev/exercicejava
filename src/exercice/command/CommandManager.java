package exercice.command;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Classe permettant des stocker et gérer l'execution des commandes.
 * 
 * @author Quentin
 *
 */
public class CommandManager {

	private final Map<String, Command> commands;

	/**
	 * Constructeur
	 */
	public CommandManager() {
		commands = new HashMap<>();
	}

	/**
	 * Fonction permettant de mettre sur écoute le canal d'entrées. Le programme va
	 * lire et executer les commandes enregistrées qui seront reseignées dans ce
	 * canal.
	 * 
	 * @param scanner Le 'scanneur' d'entrées, qui va informer le système à chaque
	 *                nouvelle entrée dans la console.
	 */
	public void listenEntry(Scanner scanner) {
		String entry = scanner.nextLine();

		String[] tab = entry.split(" ");

		onCommand(tab[0], generateCopyOfArgsFrom(tab, 1, new String[] {}));
		listenEntry(scanner);
	}

	/**
	 * Fonction permettant d'enregistrer une commande.
	 * 
	 * @param name         Le nom de la commande.
	 * @param desc         La description de la commande.
	 * @param cmdArgs      Les arguments que la commande doit demander.
	 * @param optionalArgs Les arguments optionels que la commande peut comprendre.
	 * @param executor     L'Interface Fonctionnelle permettant de contenir
	 *                     l'action.
	 * @return L'objet de la commande enregistrée.
	 */
	public Command register(String name, String desc, Set<String> cmdArgs, Set<String> optionalArgs,
			CommandExecutor executor) {
		Command command = new Command(desc, cmdArgs, optionalArgs, executor);
		System.out.println("Commande " + name + " enregistrée.");
		this.commands.put(name, command);
		return command;
	}

	/**
	 * Fonction permettant d'analyser une entrée et de lancer l'execution d'une
	 * commande existante.
	 * 
	 * @param command Le nom de la commande entré.
	 * @param args    Les arguments suivant la commande.
	 */
	public void onCommand(String command, String[] args) {
		for (String name : commands.keySet()) {
			Command obj = commands.get(name);

			if (args.length > 0) {
				for (String sub : obj.getSubCommands().keySet()) {
					if (args[0].equalsIgnoreCase(sub)) {
						executeCommand(command + " " + sub, obj.getSubCommands().get(sub),
								generateCopyOfArgsFrom(args, 1, new String[] {}));
						return;
					}
				}
			}

			if (name.equalsIgnoreCase(command)) {
				executeCommand(command, obj, args);
			}
		}
	}

	/**
	 * Fonction permettant d'executer une commande.
	 * 
	 * @param commandName Le nom de la commande.
	 * @param command     L'objet représentant la commande.
	 * @param args        Les arguments renseignés et lus.
	 */
	private void executeCommand(String commandName, Command command, String[] args) {

		if (args.length >= command.getMinArgs()) {
			command.getExecutor().execute(args);
			return;
		}

		System.err.println("> Utilisation: " + commandName + " " + command.generateSyntax());
		if (!command.getSubCommands().isEmpty()) {
			String separator = ", ";
			StringBuilder subCommands = new StringBuilder("> Sous-commandes: ");
			for (String subName : command.getSubCommands().keySet()) {
				subCommands.append(subName);
				subCommands.append(separator);
			}
			System.err.println(subCommands.substring(0, subCommands.length() - separator.length()));
		}
	}

	/**
	 * Fonction utilitaire permettant que recréer un tableau de valeur faisant la
	 * taille demandée.
	 * 
	 * @param <T>         Le type de tableau à recréer.
	 * @param obj         L'objet du tableau à recréer.
	 * @param from        L'index initial des valeurs à copier.
	 * @param newInstance La nouvelle instance au cas où le tableau de valeurs doit
	 *                    être vide.
	 * @return Un nouveau tableau contenant les données que l'on souhaite conserver.
	 */
	private <T> T[] generateCopyOfArgsFrom(T[] obj, int from, T[] newInstance) {
		return ((T[]) (obj.length > 1 ? Arrays.copyOfRange(obj, from, obj.length) : newInstance));
	}
}
