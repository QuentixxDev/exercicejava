package exercice.command;

import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Classe principale.
 * 
 * @author Quentin
 *
 */
public class Main {

	/**
	 * Fonction executée au démarrage du programme.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		CommandManager manager = new CommandManager();

		registerCommands(manager);

		manager.listenEntry(scanner);
	}

	/**
	 * Envoyer un message style console. (plus esthétique)
	 * 
	 * @param msg Le message qui sera envoyé.
	 */
	private static void printMessage(String msg) {
		System.out.println("> " + msg);
	}

	/**
	 * Charger les commandes.
	 * 
	 * @param manager Le gestionnaire des commandes.
	 */
	private static void registerCommands(CommandManager manager) {

		// Command principale
		Set<String> cmdArgs = new HashSet<>();
		cmdArgs.add("Nombre");
		Set<String> optArgs = new HashSet<>();
		Command commandMain = manager.register("main", "Afficher le nombre entré", cmdArgs, optArgs, args -> {
			try {
				printMessage("" + Integer.parseInt(args[0]));
			} catch (NumberFormatException e) {
				System.err.println("> Vous devez renseigner une valeur numérique.");
			}
		});

		// Deuxième commande (qui est une sous commande de la précédente)
		cmdArgs = new HashSet<>();
		cmdArgs.add("Nombre");

		optArgs = new HashSet<>();
		optArgs.add("Second nombre");
		Command command2nd = new Command("Afficher les nombres entrés, deuxième nombre optionel, par défaut 0", cmdArgs,
				optArgs, args -> {
					try {
						int firstValue = Integer.parseInt(args[0]);
						int secondOptionalValue = args.length > 1 ? Integer.parseInt(args[1]) : 0;
						printMessage(firstValue + " " + secondOptionalValue);
					} catch (NumberFormatException e) {
						System.err.println("> Vous devez renseigner une valeur numérique.");
					}
				});
		commandMain.addSubCommand("2nd", command2nd);

		// Troisième commande (qui est une sous commande de la première) - Afficher
		// syntaxes
		cmdArgs = new HashSet<>();
		optArgs = new HashSet<>();
		Command commandHelp = new Command("Aide relative à la commande main (sous-commandes, syntaxes)", cmdArgs,
				optArgs, args -> {

					Map<String, Command> subCommands = commandMain.getSubCommands();

					printSyntax("main", commandMain);

					for (String subCommandName : subCommands.keySet()) {

						printSyntax("main " + subCommandName, subCommands.get(subCommandName));
					}

				});
		commandMain.addSubCommand("help", commandHelp);
	}

	/**
	 * Afficher la syntaxe main d'une commande dans la console.
	 * 
	 * @param commandName Le nom de la commande.
	 * @param command     L'objet représentant la commande.
	 */
	private static void printSyntax(String commandName, Command command) {
		StringBuilder syntax = new StringBuilder(commandName);
		syntax.append(" - ");
		syntax.append(command.getCommandDesc());
		if (command.getMinArgs() > 0) {
			syntax.append("\n> ");
			syntax.append(commandName);
			syntax.append(' ');
			syntax.append(command.generateSyntax());
		}
		syntax.append("\n> ");

		printMessage(syntax.toString());
	}

}
